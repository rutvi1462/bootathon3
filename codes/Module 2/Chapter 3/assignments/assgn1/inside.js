var a1 = document.getElementById("a1");
var a2 = document.getElementById("a2");
var b1 = document.getElementById("b1");
var b2 = document.getElementById("b2");
var c1 = document.getElementById("c1");
var c2 = document.getElementById("c2");
var p1 = document.getElementById("p1");
var p2 = document.getElementById("p2");
var ans = document.getElementById("ans");
function inside() {
    var x1 = parseFloat(a1.value);
    var y1 = parseFloat(a2.value);
    var x2 = parseFloat(b1.value);
    var y2 = parseFloat(b2.value);
    var x3 = parseFloat(c1.value);
    var y3 = parseFloat(c2.value);
    var x4 = parseFloat(p1.value);
    var y4 = parseFloat(p1.value);
    console.log(x1, x2, x3, y1, y2, y3, x4, y4);
    var A = area(x1, y1, x2, y2, x3, y3); //calculates the area of triangle ABC
    var A1 = area(x1, y1, x2, y2, x4, y4); //calculates the area of triangle PAB
    var A2 = area(x4, y4, x2, y2, x3, y3); //calculates the area of triangle PBC
    var A3 = area(x1, y1, x4, y4, x3, y3); //calculates the area of triangle PAC
    console.log(A);
    console.log(A1 + A2 + A3);
    if (Math.abs(A - (A1 + A2 + A3)) < 0.000001) {
        ans.value = "Point P is inside the triangle ABC";
    }
    else {
        ans.value = "Point P is outside the triangle ABC";
    }
}
let area = function (x1, y1, x2, y2, x3, y3) {
    return (Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2));
};
//# sourceMappingURL=inside.js.map