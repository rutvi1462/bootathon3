var count : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
var tab : HTMLTableElement = <HTMLTableElement>document.getElementById("tab");

function mult()
{
    var t1 :number= +(count.value);   //typecasting string to integer value
    while(tab.rows.length > 1)
    {
        tab.deleteRow(1);
    }

    console.log(t1);

    for(let i=1; i<=t1;i++ )
    {
        var row: HTMLTableRowElement = tab.insertRow();  //inserts a row
        var cell1 : HTMLTableDataCellElement = row.insertCell(); //inserts a cell in the row
        var cell2 : HTMLTableDataCellElement = row.insertCell();
        var cell3 : HTMLTableDataCellElement = row.insertCell();

        cell1.innerHTML = t1.toString() + "*" + i.toString(); //assigns value to cell
        cell2.innerHTML = "=";
        cell3.innerHTML = (t1*i).toString();
    }
}
